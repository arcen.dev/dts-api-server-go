package mahasiswa

import (
	"activity/config"
	"activity/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"
)

//AriSuseno
const (
	table          = "mahasiswa"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {
	var mahasiswas []models.Mahasiswa

	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryTest := fmt.Sprintf("SELECT * FROM %v ORDER BY id DESC", table)

	rowQuery, err := db.QueryContext(ctx, queryTest)
	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa models.Mahasiswa
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil {
			log.Fatal(err)
		}

		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}
		mahasiswas = append(mahasiswas, mahasiswa)
	}
	return mahasiswas, nil
}

func Insert(ctx context.Context, mhs models.Mahasiswa) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	// queryText := fmt.Sprintf("INSERT INTO %v (nim, name, semester, created_at, updated_at) values(%v,'%v',%v,'%v','%v')", table,
	queryText := fmt.Sprintf("INSERT INTO %v (nim, name, semester) values(%v,'%v','%v')",
		table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester)
	// time.Now().Format(layoutDateTime),
	// time.Now().Format(layoutDateTime))

	fmt.Println(queryText)
	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, mhs models.Mahasiswa) (error, int64) {

	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nim = %d, name ='%s', semester = %d, updated_at = '%v' where id = '%d'",
		table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		time.Now().Format(layoutDateTime),
		mhs.ID,
	)

	s, err := db.ExecContext(ctx, queryText)
	if err != nil {
		return err, 0
	}

	check, err := s.RowsAffected()
	if check == 0 {
		if mhs.ID == 0 {
			return errors.New("ID Tidak Ditemukan"), 0
		}
		fmt.Println("cek nim", mhs.ID)
		return errors.New("Data tidak valid"), 0

	}

	fmt.Println(strconv.FormatInt(check, 10))

	return nil, check
}

func Delete(ctx context.Context, mhs models.Mahasiswa) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %d", table, mhs.ID)
	s, err := db.ExecContext(ctx, queryText)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	fmt.Println("cek del :", queryText)

	check, err := s.RowsAffected()
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}

func Get(ctx context.Context, mhs models.Mahasiswa) ([]models.Mahasiswa, error) {
	var mahasiswas []models.Mahasiswa

	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryTest := fmt.Sprintf("SELECT * FROM %v WHERE id = %d ORDER BY id DESC", table, mhs.ID)

	rowQuery, err := db.QueryContext(ctx, queryTest)
	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa models.Mahasiswa
		var createdAt, updatedAt string

		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil {
			log.Fatal(err)
		}

		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}
		mahasiswas = append(mahasiswas, mahasiswa)
	}
	return mahasiswas, nil
}
