//AriSuseno
package main

import (
	"activity/config" //activity = nama project
	"activity/mahasiswa"
	"activity/models"
	"activity/utils"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

//AriSuseno
func main() {
	//Routing Method
	http.HandleFunc("/mahasiswa", GetMahasiswa)
	http.HandleFunc("/mahasiswa/create", PostMahasiswa)
	http.HandleFunc("/mahasiswa/update", UpdateMahasiswa)
	http.HandleFunc("/mahasiswa/delete", DeleteMahasiswa)
	err := http.ListenAndServe(":4121", nil)
	if err != nil {
		log.Fatal(err)
	}

	//Config MySQL
	db, e := config.MySQL()
	if e != nil {
		log.Fatal(e)
	}

	eb := db.Ping()
	if eb != nil {
		panic(eb.Error())
	}
	fmt.Println("Success")

	err = http.ListenAndServe(":7000", nil)
	if err != nil {
		log.Fatal(err)
	}
}

//Get Mahasiswa
func GetMahasiswa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		mahasiswas, err := mahasiswa.GetAll(ctx)
		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, mahasiswas, http.StatusOK)
		return
	}

	http.Error(w, "Tidak diizinkan", http.StatusNotFound)
	return
}

//Post Mahasiswa
func PostMahasiswa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var mhs models.Mahasiswa
		if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		if err := mahasiswa.Insert(ctx, mhs); err != nil {
			utils.ResponseJSON(w, err, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusCreated)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}

// UpdateMahasiswa
func UpdateMahasiswa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "PUT" {
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var mhs models.Mahasiswa
		if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		if err, updateCount := mahasiswa.Update(ctx, mhs); err != nil {
			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}
			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		} else {
			res := map[string]string{
				"status": "Data berhasil di Update",
			}

			update := map[string]int64{
				"update": updateCount,
			}
			utils.ResponseJSON(w, res, http.StatusCreated)
			utils.ResponseJSON(w, update, http.StatusCreated)
		}
		return
	}
	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}

// DeleteMahasisw
func DeleteMahasiswa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "DELETE" {
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var mhs models.Mahasiswa
		if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		//Get data By ID
		rescek, cek := mahasiswa.Get(ctx, mhs)
		if cek != nil {
			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", cek),
			}
			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		if err := mahasiswa.Delete(ctx, mhs); err != nil {
			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}
			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]interface{}{
			"Data berhasil di Hapus ": rescek,
		}

		utils.ResponseJSON(w, res, http.StatusOK)
		return

	}
	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}
